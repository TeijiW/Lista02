public class L2Q8{
    public static void main(String[] args){

        System.out.println("");
        System.out.println("");
        System.out.println("Texto 1 com System.out.println()");
        System.out.println("Texto 2 com System.out.println()");
        System.out.println("");
        System.out.println("----------------------");
        System.out.println("");
        System.out.print("Texto 1 com System.out.print()");
        System.out.print("Texto 2 com System.out.print()");
        System.out.println("");
        System.out.println("");
    }
}
