import java.util.Scanner; 

public class L2Q6{
    public static void main (String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Insira um valor qualquer: ");
        int n = input.nextInt();
        System.out.println("====================");
        System.out.println("Sucessor: " + ++n);
        n--;
        System.out.println("Antecessor: " + --n);
    }
}